<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Akron</title>

        <script src="src/libs/jquery3.3.1.min.js"></script>
        <script src="src/libs/popper.min.js"></script>
        <script src="src/libs/bootstrap.min.js"></script>

        <link rel="stylesheet" href="src/libs/bootstrap.min.css">
        <link rel="stylesheet" href="src/libs/all.css">
        <link rel="stylesheet" href="src/libs/styles.css">
    </head>
    <body>
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-md-1 text-center">
                    <img src="src/assets/images/mini_logo.png">
                </div>
                <div class="col-md-11 text-center">
                    <img src="src/assets/images/Logo.png">
                </div>
                <div class="col-md-12 text-center">
                    <div class="row justify-content-center mt-5">
                        <div class="col-md-2 akron-bckgrd rounded mt-5">
                            <form action="src/main/controladores/main.php">
                                <div class="form-group mt-3">
                                    <label style="color: white">Usuario</label>
                                    <input type="text" class="form-control" id="usuario" placeholder="Ingrese Usuario">
                                </div>
                                <div class="form-group">
                                    <label style="color: white">Clave</label>
                                    <input type="password" class="form-control" id="clave" placeholder="Ingrese Clave">
                                </div>
                                <button type="submit" class="btn btn-success mb-3">Ingresar</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
