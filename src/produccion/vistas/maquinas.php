<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Akron</title>

        <!-- LIBRARIES -->
        <?php include '../../includes/libraries.php' ?>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row justify-content-center">
                <!-- HEADER -->
                <?php include '../../includes/header.php' ?>
                <!-- TOP MENU -->
                <?php include '../../includes/top_menu.php' ?>
                <div class="col-md-2" id="left_menu">
                    <ul class="list-group list-group-flush">
                        <?php
                            for($i = 1 ; $i <= 6; $i ++) {
                        ?>
                        <li class="list-group-item">Inyectora <?php echo($i); ?></li>
                        <?php
                            }
                        ?>
                        <?php
                        for($i = 1 ; $i <= 6; $i ++) {
                            ?>
                            <li class="list-group-item">Prensa China<?php echo($i); ?></li>
                            <?php
                        }
                        ?>
                    </ul>
                </div>
                <div class="col-md-10 align-content-center">
                    <div class="row text-center mt-5 ml-5">
                        <?php
                            for($i = 1 ; $i <= 6; $i ++) {
                        ?>
                        <div class="col-md-2 mb-3">
                            <div class="card" style="width: 8rem;">
                                <img src="../../assets/images/inyectora.png" class="card-img-top" alt="...">
                                <div class="card-body">
                                    <p class="card-text">Inyectora <?php echo($i); ?></p>
                                </div>
                            </div>
                        </div>
                                <?php
                            }
                        ?>
                        <?php
                            for($i = 1 ; $i <= 6; $i ++) {
                        ?>
                        <div class="col-md-2 mt-3">
                            <div class="card" style="width: 8rem;">
                                <img src="../../assets/images/prensa-china.png" class="card-img-top" alt="...">
                                <div class="card-body">
                                    <p class="card-text">Prensa China <?php echo($i); ?></p>
                                </div>
                            </div>
                        </div>
                        <?php
                            }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>