<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Akron</title>

        <!-- LIBRARIES -->
        <?php include '../../includes/libraries.php' ?>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row justify-content-center">
                <!-- HEADER -->
                <?php include '../../includes/header.php' ?>
                <!-- TOP MENU -->
                <?php include '../../includes/top_menu.php' ?>
                <div class="col-md-2" id="left_menu">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">Molino</li>
                        <li class="list-group-item">Vulcanizado</li>
                        <li class="list-group-item">Inyecci&oacute;n</li>
                        <li class="list-group-item">Prensado</li>
                        <li class="list-group-item">Corte</li>
                        <li class="list-group-item">Lijado</li>
                        <li class="list-group-item">Desconche</li>
                    </ul>
                </div>
                <div class="col-md-10">

                </div>
            </div>
        </div>
    </body>
</html>