<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Akron</title>

        <!-- LIBRARIES -->
        <?php include '../../includes/libraries.php' ?>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row justify-content-center">
                <!-- HEADER -->
                <?php include '../../includes/header.php' ?>
                <!-- TOP MENU -->
                <?php include '../../includes/top_menu.php' ?>
                <div class="col-md-2" id="left_menu">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">Desde Profit</li>
                        <li class="list-group-item">Desde Archivo</li>
                        <li class="list-group-item">Detallado</li>
                        <li class="list-group-item">Devoluci&oacute;n a Stock</li>
                        <li class="list-group-item">Buscar</li>
                    </ul>
                </div>
                <div class="col-md-10">

                </div>
            </div>
        </div>
    </body>
</html>