<div class="col-md-12 text-center" id="div_top_menu">
    <ul class="d-inline-flex first-level-menu">
        <li><span><a href="../../main/controladores/main.php">Inicio</a></span></li>
        <li>
            <span>Producci&oacute;n</span>&nbsp;<i class="fas fa-chevron-down"></i>
            <ul class="second-level-menu">
                <li><span class="ml-1"><a href="../../produccion/controladores/orden_trab_main.php">Orden de Trabajo</a></span></li>
                <li><span class="ml-1"><a href="../../produccion/controladores/maquinas.php">M&aacute;quinas</a></span></li>
                <li><span class="ml-1"><a href="../../produccion/controladores/procesos_main.php">Procesos</a></span></li>
                <li><span class="ml-1"><a href="../../produccion/controladores/control_prod_main.php">Control Productivo</a></span></li>
            </ul>
        </li>
        <li>
            <span>Almac&eacute;n</span>&nbsp;<i class="fas fa-chevron-down"></i>
            <ul class="second-level-menu">
                <li><span class="ml-1"><a href="../../almacen/controladores/programaciones_main.php">Programaciones</a></span></li>
                <li><span class="ml-1"><a href="../../almacen/controladores/producto_en_proceso_main.php">Producto en Proceso</a></span></li>
                <li><span class="ml-1"><a href="../../almacen/controladores/ensamble_main.php">Ensamble</a></span></li>
                <li><span class="ml-1"><a href="../../almacen/controladores/productos_main.php">Productos</a></span></li>
                <li><span class="ml-1"><a href="">Reportes</a></span></li>
                <li><span class="ml-1"><a href="">Ingresos</a></span></li>
                <li><span class="ml-1"><a href="../../almacen/controladores/inventarios_main.php">Inventarios</a></span></li>
            </ul>
        </li>
        <li>
            <span>I+D</span>&nbsp;<i class="fas fa-chevron-down"></i>
            <ul class="second-level-menu">
                <li><span class="ml-1">Gesti&oacute;n Moldes</span></li>
                <li><span class="ml-1">Gesti&oacute;n M&aacute;quinas</span></li>
                <li><span class="ml-1">Gesti&oacute;n Bases</span></li>
                <li><span class="ml-1">Gesti&oacute;n Pastas</span></li>
                <li><span class="ml-1">Gesti&oacute;n Gomas</span></li>
                <li><span class="ml-1">Gesti&oacute;n Granel</span></li>
                <li><span class="ml-1">Gesti&oacute;n Kits</span></li>
            </ul>
        </li>
        <li>
            <span>Comercializaci&oacute;n</span>&nbsp;<i class="fas fa-chevron-down"></i>
            <ul class="second-level-menu">
                <li><span class="ml-1">Gesti&oacute;n Clientes</span></li>
                <li><span class="ml-1">Gesti&oacute;n Proveedores</span></li>
                <li><span class="ml-1"><a href="../../comercializacion/controladores/gest_pedidos_main.php">Gesti&oacute;n Pedidos</a></span></li>
                <li><span class="ml-1">Gesti&oacute;n Orden Compra</span></li>
                <li><span class="ml-1"><a href="../../comercializacion/controladores/programaciones_main.php">Programaciones</a></span></li>
                <li><span class="ml-1"><a href="../../comercializacion/controladores/productos_main.php">Productos</a></span></li>
                <li><span class="ml-1"><a href="">Generar Etiquetas</a></span></li>
            </ul>
        </li>
        <li>
            <span>Control de Calidad</span>&nbsp;<i class="fas fa-chevron-down"></i>
            <ul class="second-level-menu">
                <li><span class="ml-1">Auditor&iacute;a</span></li>
                <li><span class="ml-1">Estad&iacute;stica</span></li>
                <li><span class="ml-1">Indicadores</span></li>
                <li><span class="ml-1">Reportes</span></li>
            </ul>
        </li>
        <li>
            <span>Administraci&oacute;n</span>&nbsp;<i class="fas fa-chevron-down"></i>
            <ul class="second-level-menu">
                <li><span class="ml-1">Reportes</span></li>
                <li><span class="ml-1">N&oacute;mina</span></li>
                <li><span class="ml-1">Reportes QA</span></li>
            </ul>
        </li>
        <li>
            <span>Sistema</span>&nbsp;<i class="fas fa-chevron-down"></i>
            <ul class="second-level-menu">
                <li><span class="ml-1"><a href="../../sistema/controladores/usuarios_listar.php">Gestion Usuario</a></span></li>
                <li><span class="ml-1"><a href="../../sistema/controladores/roles_listar.php">Gestion Roles</a></span></li>
            </ul>
        </li>
    </ul>
</div>