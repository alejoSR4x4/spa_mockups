<?php

    $total = 100;
    $itemsByPage = 10;
    $totalPages = ceil($total/$itemsByPage);
?>
<nav aria-label="Page navigation example">
    <ul class="pagination justify-content-center">
        <li class="page-item">
            <a class="page-link" href="#" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a>
        </li>
        <li class="page-item active"><a class="page-link" href="?pag=1">1</a></li>
        <?php for ($j = 1; $j <= 9 ; $j++) {?>
            <li class="page-item"><a class="page-link" href="?pag=<?php echo($j+1);?>"><?php echo($j+1);?></a></li>
            <?php
        }
        ?>
        <li class="page-item">
            <a class="page-link" href="#" aria-label="Next"><span aria-hidden="true">&raquo;</span></a>
        </li>
    </ul>
</nav>
