<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Akron</title>

        <!-- LIBRARIES -->
        <?php include '../../includes/libraries.php' ?>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row justify-content-center">
                <!-- HEADER -->
                <?php include '../../includes/header.php' ?>
                <!-- TOP MENU -->
                <?php include '../../includes/top_menu.php' ?>
                <div class="col-md-6">
                    <div class="mt-3">
                        <table class="table table-bordered">
                            <thead style="background-color: #28a745; color: white">
                                <tr><th colspan="2" class="text-center"> Actualizar Role</th></tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Nombre Rol:</td>
                                    <td><input placeholder="Ingrese Nombre del Rol"></td>
                                </tr>
                                <tr>
                                    <td class="align-middle">Permisos</td>
                                    <td>
                                        <div>
                                            <input type="checkbox"><span>Produccion</span><br>
                                            <input class="ml-3" type="checkbox"><span>Programaciones</span><br>
                                            <input class="ml-3" type="checkbox"><span>Pedidos</span><br>
                                            <input class="ml-3" type="checkbox"><span>Procesos</span><br>
                                            <input type="checkbox"><span>Sistema</span><br>
                                            <input class="ml-3" type="checkbox"><span>Usuarios</span><br>
                                            <input class="ml-5" type="checkbox"><span>Listar</span><br>
                                            <input class="ml-5" type="checkbox"><span>Crear</span><br>
                                            <input class="ml-3" type="checkbox"><span>Roles</span><br>
                                            <input class="ml-5" type="checkbox"><span>Listar</span><br>
                                            <input class="ml-5" type="checkbox"><span>Crear</span>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="text-center">
                                        <button type="submit" class="btn btn-primary">Actualizar</button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
