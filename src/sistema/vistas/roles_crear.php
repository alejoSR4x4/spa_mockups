<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Akron</title>

        <!-- LIBRARIES -->
        <?php include '../../includes/libraries.php' ?>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row justify-content-center">
                <!-- HEADER -->
                <?php include '../../includes/header.php' ?>
                <!-- TOP MENU -->
                <?php include '../../includes/top_menu.php' ?>
                <div class="col-md-9">
                    <div class="mt-3">
                        <table class="table table-bordered">
                            <thead style="background-color: #28a745; color: white">
                                <tr><th colspan="2" class="text-center"> Crear Role</th></tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Nombre Rol:</td>
                                    <td><input placeholder="Ingrese Nombre del Rol"></td>
                                </tr>
                                <tr>
                                    <td class="align-middle">Permisos a Asignar</td>
                                    <td>
                                        <div class="row">
                                            <div id="menu_primer_nivel" class="col-md-5">
                                                <h5>Primer Nivel de Permisos</h5>
                                                <input type="checkbox"><span>Produccion</span><br>
                                                <input type="checkbox"><span>Almac&eacute;n</span><br>
                                                <input type="checkbox"><span>I+D</span><br>
                                                <input type="checkbox"><span>Comercializaci&oacute;n</span><br>
                                                <input type="checkbox"><span>Control de Calidad</span><br>
                                                <input type="checkbox"><span>Administraci&oacute;n</span><br>
                                                <input type="checkbox"><span>Sistema</span>
                                            </div>
                                            <div id="menu_segundo_nivel" class="col-md-4 invisible d-none">
                                                <h5>Segundo Nivel de Permisos</h5>
                                                <input type="checkbox"><span>Orden de Trabajo</span><br>
                                                <input type="checkbox"><span>M&aacute;quinas</span><br>
                                                <input type="checkbox"><span>Procesos</span><br>
                                                <input type="checkbox"><span>Control Productivo</span>
                                            </div>
                                            <div id="menu_tercer_nivel" class="col-md-4 invisible d-none">
                                                <h5>Tercer Nivel de Permisos</h5>
                                                <input type="checkbox"><span>Molino</span><br>
                                                <input type="checkbox"><span>Vulcanizado</span><br>
                                                <input type="checkbox"><span>Inyecci&oacute;n</span><br>
                                                <input type="checkbox"><span>Prensado</span><br>
                                                <input type="checkbox"><span>Corte</span><br>
                                                <input type="checkbox"><span>Lijado</span><br>
                                                <input type="checkbox"><span>Desconche</span><br>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="text-center">
                                        <button type="submit" class="btn btn-primary">Crear</button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script type="application/javascript">

        $('div#menu_primer_nivel input[type=checkbox]').click(function () {
            $('#menu_segundo_nivel').removeClass('invisible d-none');
            $('#menu_segundo_nivel').addClass('visible d-block');
            $('#menu_primer_nivel').removeClass('col-md-5');
            $('#menu_primer_nivel').addClass('col-md-4');

        });

        $('div#menu_segundo_nivel input[type=checkbox]').click(function () {
            $('#menu_tercer_nivel').removeClass('invisible d-none');
            $('#menu_tercer_nivel').addClass('visible d-block');
        });
    </script>
</html>
