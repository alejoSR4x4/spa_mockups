<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Akron</title>

        <!-- LIBRARIES -->
        <?php include '../../includes/libraries.php' ?>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row justify-content-center">
                <!-- HEADER -->
                <?php include '../../includes/header.php' ?>
                <!-- TOP MENU -->
                <?php include '../../includes/top_menu.php' ?>
                <div class="col-md-6">
                    <div class="mt-3">
                        <table class="table table-bordered">
                            <thead style="background-color: #28a745; color: white">
                                <tr><th colspan="2" class="text-center"> Crear Usuario</th></tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Nombre Usuario:</td>
                                    <td><input placeholder="Ingrese Nombre del Usuario"></td>
                                </tr>
                                <tr>
                                    <td>C&eacute;dula Usuario:</td>
                                    <td><input placeholder="Ingrese C&eacute;dula del Usuario"></td>
                                </tr>
                                <tr>
                                    <td>Alias Usuario:</td>
                                    <td><input placeholder="Ingrese Alias del Usuario"></td>
                                </tr>
                                <tr>
                                    <td class="align-middle">Rol</td>
                                    <td>
                                        <div>
                                            <input type="radio" name="rol"><span>Rol 1</span><br>
                                            <input type="radio" name="rol"><span>Rol 2</span><br>
                                            <input type="radio" name="rol"><span>Rol 3</span><br>
                                            <input type="radio" name="rol"><span>Rol 4</span><br>
                                            <input type="radio" name="rol"><span>Rol 5</span><br>
                                            <input type="radio" name="rol"><span>Rol 6</span><br>
                                            <input type="radio" name="rol"><span>Rol 7</span><br>
                                            <input type="radio" name="rol"><span>Rol 8</span><br>
                                            <input type="radio" name="rol"><span>Rol 9</span><br>
                                            <input type="radio" name="rol"><span>Rol 10</span><br>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="text-center">
                                        <button type="submit" class="btn btn-primary">Crear</button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
