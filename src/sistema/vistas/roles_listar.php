<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Akron</title>

        <!-- LIBRARIES -->
        <?php include '../../includes/libraries.php' ?>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row justify-content-center">
                <!-- HEADER -->
                <?php include '../../includes/header.php' ?>
                <!-- TOP MENU -->
                <?php include '../../includes/top_menu.php' ?>
                <div class="col-md-6">
                    <div class="mt-3">
                        <div class="text-right">
                            <button class="btn btn-primary mb-3" id="addRole">
                                <a href="../controladores/roles_crear.php">Agregar</a>
                            </button>
                        </div>
                        <div class="text-center">
                            <table class="table table-sm table-hover">
                                <thead style="background-color: #28a745; color: white">
                                <tr>
                                    <th class="text-center">Rol</th>
                                    <th class="text-center">M&oacute;dulos</th>
                                    <th class="text-center">Opciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                for($i = 0 ; $i < 10 ; $i++){
                                    ?>
                                    <tr>
                                        <td class="text-center align-middle">Rol <?php echo($i+1); ?></td>
                                        <td>
                                            <div>
                                                <span class="badge-pill badge-info">Usuarios</span>
                                                <span class="ml-2 badge-pill badge-info">Roles</span>
                                                <span class="ml-2 badge-pill badge-info">Producci&oacute;n</span>
                                                <span class="ml-2 badge-pill badge-info">Almac&eacute;n</span>
                                                <span class="ml-2 badge-pill badge-info">Roles</span>
                                            </div>
                                        </td>
                                        <td class="text-center align-middle">
                                            <a href="../controladores/roles_actualizar.php?id=<?php echo($i+1); ?>">
                                                <i class="fas fa-pencil-alt ml-3" style="color: black"></i>
                                            </a>
                                            <i class="far fa-trash-alt ml-3"></i>
                                        </td>
                                    </tr>
                                    <?php
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                        <div>
                            <?php include("../../includes/paginador.php") ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
